<img align="right" width="100" height="74" src="https://user-images.githubusercontent.com/8277210/183290025-d7b24277-dfb4-4ce1-bece-7fe0ecd5efd4.svg" />

# Report monorepo services to Port with GitLab CI

## Setup

To set up and use this workflow, follow these steps:

## 1. Add Environment Variables to CI/CD

Add the `PORT_CLIENT_ID` and `PORT_CLIENT_SECRET` variables to your CI/CD environment settings. These variables are required for the script to authenticate with the Port API.

## 2. Create a Blueprint in Port

Create a new blueprint in Port with the following JSON configuration:

```json
{
  "identifier": "microservice",
  "title": "Microservice",
  "icon": "Git",
  "schema": {
    "properties": {
      "url": {
        "title": "URL",
        "type": "string",
        "icon": "GitLab",
        "format": "url"
      },
      "readme": {
        "icon": "Star",
        "title": "readme",
        "description": "markd",
        "type": "string",
        "format": "markdown"
      }
    },
    "required": []
  },
  "mirrorProperties": {},
  "calculationProperties": {},
  "relations": {}
}
```

This blueprint defines the structure of the microservices, including their properties and relations.

## 3. Configure the Scanned Folder

By default, the script scans the `apps` folder. If you want to change the folder to be scanned, update the folder name in the `find` command within the script:

```bash
for folder in $(find <your_folder>/*/ -maxdepth 0 -type d); do
```

Replace `<your_folder>` with the desired folder name.

## 4. Update the Modified URL

Change the `modified_url` variable in the script to match the URL of your monorepo. Update the following line:

```bash
modified_url="https://gitlab.com/getport-labs/test-monorepo/-/tree/apps/$identifier"
```

Replace the URL with the appropriate base URL for your monorepo.

## 5. That's it!

Copy the `.gitlab-ci.yml` to your monorepo and start working 

## Workflow Explanation

This workflow aims to report microservices to Port. It's a simple CI/CD pipeline that scans a GitLab repository, extracts information about the microservices, and sends them to Port using Port's API.

## Stages

There is only one stage in this workflow: `report_microservices_to_port`.

## Before Script

The `before_script` section installs the required dependencies for the script, specifically `jq`, a command-line JSON processor.

```bash
apt-get update -qq && apt-get install -y -qq jq
```

## report_microservices_to_port Stage

This stage is responsible for extracting information from the GitLab repository and reporting it to Port. The script does the following:

1. Obtain an access token from Port's API using the `PORT_CLIENT_ID` and `PORT_CLIENT_SECRET`.
2. Iterate through each folder in the `apps` directory.
3. For each folder, extract the folder name, which will be used as the identifier and title.
4. Find and read the contents of the `README.md` file (if it exists).
5. Construct a modified URL pointing to the microservice's folder in the GitLab repository.
6. Send a POST request to Port's API, including the identifier, title, modified URL, and readme content.

Here's the complete code block for the pipeline:

```yaml
stages:
  - report_microservices_to_port

before_script:
  - apt-get update -qq && apt-get install -y -qq jq

report_microservices_to_port:
  stage: report_microservices_to_port
  script:
    - |
      access_token=$(curl --location --request POST 'https://api.getport.io/v1/auth/access_token' --header 'Content-Type: application/json' --data-raw '{"clientId": "'"$PORT_CLIENT_ID"'","clientSecret": "'"$PORT_CLIENT_SECRET"'"}' | jq '.accessToken' | sed 's/"//g')
      for folder in $(find apps/*/ -maxdepth 0 -type d); do
          identifier=$(basename "$folder")
          title=$(basename "$folder")
          readme_path=$(find "$folder" -iname 'README.md' | head -n 1)
          readme_content=""
          if [ -f "$readme_path" ]; then
            readme_content=$(awk '{printf("%s\\n", $0)}' "$readme_path" | tr '\n' '|' | sed 's/|/\\n/g')
          fi
          modified_url="https://gitlab.com/getport-labs/test-monorepo/-/tree/apps/$identifier"
          curl --location --request POST "https://api.getport.io/v1/blueprints/gitlab-microservice/entities?upsert=true" \
            --header "Authorization: Bearer $access_token" \
            --header "Content-Type: application/json" \
            --data-raw '{
              "identifier": "'"$identifier"'",
              "title": "'"$title"'",
              "properties": {"url":"'"$modified_url"'","readme":"'"$readme_content"'"},
              "relations": {}
            }';
      done
```
